import sys
import numpy as np
import pandas as pd 




dir_name = sys.argv[1]

file1 = dir_name+'LOGS1/history.data'
file2 = dir_name+'LOGS2/history.data'


print (" ")#Primary history file: %s"%(file1))
print (" ")#Secondary history file: %s"%(file2))

###LOAD DATA FILES
data1 = pd.read_csv(file1,skiprows=5,\
                    delim_whitespace=True,\
                    usecols=['star_age','star_mass','log_Teff','log_L','log_R','log_g','center_h1','center_he4','center_c12','lg_mtransfer_rate',\
                             'surf_avg_omega_div_omega_crit','surface_h1','surface_he4','surface_c12','surface_n14','surf_avg_v_rot','v_orb_1',\
                             'rl_relative_overflow_1','rl_relative_overflow_2','he_core_mass','c_core_mass','period_days','binary_separation'])        
data1['star_age'] = data1['star_age']/1000000
file2=dir_name+'/LOGS2/history.data'
data2 = pd.read_csv(file2,skiprows=5,\
                    delim_whitespace=True,\
                    usecols=['star_age','star_mass','log_Teff','log_L','log_R','log_g','center_h1','center_he4','center_c12','lg_mtransfer_rate',\
                             'surf_avg_omega_div_omega_crit','surface_h1','surface_he4','surface_c12','surface_n14','surf_avg_v_rot','v_orb_2',\
                             'rl_relative_overflow_1','rl_relative_overflow_2','he_core_mass','c_core_mass','period_days','binary_separation'])
data2['star_age'] = data2['star_age']/1000000

print("  Myrs                                 days       Rs     Ms     Ms     Ls     Ls     K     K     cgs     cgs     Rs     Rs                                              ")
print("   Age    Xc1    Xc2    Yc1    Yc2     orbP        a     M1     M2   lgL1   lgL2  lgT1  lgT2    lgg1    lgg2     R1     R2   (R1/RL1)-1   (R2/RL2)-1   Ys1   Ys2   Ns1/N0   Ns2/N0   v1/v1_crit   v2/v2_crit")

###DATA AT ZAMS
print("Stellar and binary parameters at ZAMS")
print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][0],data1['center_h1'][0],data2['center_h1'][0],data1['center_he4'][0],data2['center_he4'][0],\
      data1['period_days'][0],data1['binary_separation'][0],data1['star_mass'][0],data2['star_mass'][0],\
      data1['log_L'][0],data2['log_L'][0],data1['log_Teff'][0],data2['log_Teff'][0],data1['log_g'][0],data2['log_g'][0],\
      10**(data1['log_R'][0]),10**(data2['log_R'][0]),data1['rl_relative_overflow_1'][0],data2['rl_relative_overflow_2'][0],\
      data1['surface_he4'][0],data2['surface_he4'][0],data1['surface_n14'][0]/data1['surface_n14'][0],data2['surface_n14'][0]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][0],data2['surf_avg_omega_div_omega_crit'][0]))


###FIND END OF CORE HYDROGEN BURNING OF PRIMARY
k_h = 0
end = len(data1['star_age'])
for k in range(0,end-1):
	if data1['center_h1'][k] < 0.001:
		k_h = k
		break

###CHECK IF BINARY SURVIVES TILL END OF CORE HYDROGEN BURNING OF PRIMARY
merge_ms = 0
if (k_h == 0):
	k_h = len(data1['star_age'])-1
	merge_ms = 1

###FIND IF THERE IS FAST CASE A MASS TRANSFER
fasta_start = 0
fasta_flag = 0
for k in range(0,k_h):
	if data1['lg_mtransfer_rate'][k] > -5:
		fasta_start = k
		fasta_flag = 1
		print("Fast Case A mass transfer starts")
		break

###DATA AT ONSET OF FAST CASE A MASS TRANSFER
if fasta_start > 0:
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][fasta_start],data1['center_h1'][fasta_start],data2['center_h1'][fasta_start],data1['center_he4'][fasta_start],data2['center_he4'][fasta_start],\
      data1['period_days'][fasta_start],data1['binary_separation'][fasta_start],data1['star_mass'][fasta_start],data2['star_mass'][fasta_start],\
      data1['log_L'][fasta_start],data2['log_L'][fasta_start],data1['log_Teff'][fasta_start],data2['log_Teff'][fasta_start],data1['log_g'][fasta_start],data2['log_g'][fasta_start],\
      10**(data1['log_R'][fasta_start]),10**(data2['log_R'][fasta_start]),data1['rl_relative_overflow_1'][fasta_start],data2['rl_relative_overflow_2'][fasta_start],\
      data1['surface_he4'][fasta_start],data2['surface_he4'][fasta_start],data1['surface_n14'][fasta_start]/data1['surface_n14'][0],data2['surface_n14'][fasta_start]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][fasta_start],data2['surf_avg_omega_div_omega_crit'][fasta_start]))


###FIND END OF FAST CASE A MASS TRANSFER
fasta_stop = 0
for k in range(fasta_start,k_h):
	if data1['lg_mtransfer_rate'][k] < -5:
		fasta_stop = k
		#print("Fast Case A mass transfer stops")
		break

###CHECK IF BINARY MERGES BEFORE FAST CASE A MASS TRANSFER STOPS
if (fasta_stop == 0):
	fasta_stop = k_h

###CHECK IF CONTACT OCCURS DURING FAST CASE A
contact = 0
for k in range(fasta_start,fasta_stop):
	if data1['rl_relative_overflow_1'][k] > -0.01 and data2['rl_relative_overflow_2'][k] > -0.01:
		contact = k
		print("Contact during fast Case A occurs")
		print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
	  %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
		      (data1['star_age'][contact],data1['center_h1'][contact],data2['center_h1'][contact],data1['center_he4'][contact],data2['center_he4'][contact],\
		      data1['period_days'][contact],data1['binary_separation'][contact],data1['star_mass'][contact],data2['star_mass'][contact],\
		      data1['log_L'][contact],data2['log_L'][contact],data1['log_Teff'][contact],data2['log_Teff'][contact],data1['log_g'][contact],data2['log_g'][contact],\
		      10**(data1['log_R'][contact]),10**(data2['log_R'][contact]),data1['rl_relative_overflow_1'][contact],data2['rl_relative_overflow_2'][contact],\
		      data1['surface_he4'][contact],data2['surface_he4'][contact],data1['surface_n14'][contact]/data1['surface_n14'][0],data2['surface_n14'][contact]/data2['surface_n14'][0],\
		      data1['surf_avg_omega_div_omega_crit'][contact],data2['surf_avg_omega_div_omega_crit'][contact]))
		break

###FIND IF CONTACT SEVERES DURING FAST CASE A
nocnct = 0
for k in range(contact,fasta_stop):
	if data2['rl_relative_overflow_2'][k] < -0.01 and contact > 0:
		nocnct = k
		print("Contact during fast Case A stops")
		print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
	  %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
		      (data1['star_age'][nocnct],data1['center_h1'][nocnct],data2['center_h1'][nocnct],data1['center_he4'][nocnct],data2['center_he4'][nocnct],\
		      data1['period_days'][nocnct],data1['binary_separation'][nocnct],data1['star_mass'][nocnct],data2['star_mass'][nocnct],\
		      data1['log_L'][nocnct],data2['log_L'][nocnct],data1['log_Teff'][nocnct],data2['log_Teff'][nocnct],data1['log_g'][nocnct],data2['log_g'][nocnct],\
		      10**(data1['log_R'][nocnct]),10**(data2['log_R'][nocnct]),data1['rl_relative_overflow_1'][nocnct],data2['rl_relative_overflow_2'][nocnct],\
		      data1['surface_he4'][nocnct],data2['surface_he4'][nocnct],data1['surface_n14'][nocnct]/data1['surface_n14'][0],data2['surface_n14'][nocnct]/data2['surface_n14'][0],\
		      data1['surf_avg_omega_div_omega_crit'][nocnct],data2['surf_avg_omega_div_omega_crit'][nocnct]))
		break

###IF BINARY MERGES DURING FAST CASE A MASS TRANSFER
#if (fasta_stop == k_h and fasta_flag == 1):
#	print("Binary merges during fast Case A mass transfer")
#	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
#      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
#      (data1['star_age'][fasta_stop],data1['center_h1'][fasta_stop],data2['center_h1'][fasta_stop],data1['center_he4'][fasta_stop],data2['center_he4'][fasta_stop],\
#      data1['period_days'][fasta_stop],data1['binary_separation'][fasta_stop],data1['star_mass'][fasta_stop],data2['star_mass'][fasta_stop],\
#      data1['log_L'][fasta_stop],data2['log_L'][fasta_stop],data1['log_Teff'][fasta_stop],data2['log_Teff'][fasta_stop],data1['log_g'][fasta_stop],data2['log_g'][fasta_stop],\
#      10**(data1['log_R'][fasta_stop]),10**(data2['log_R'][fasta_stop]),data1['rl_relative_overflow_1'][fasta_stop],data2['rl_relative_overflow_2'][fasta_stop],\
#      data1['surface_he4'][fasta_stop],data2['surface_he4'][fasta_stop],data1['surface_n14'][fasta_stop]/data1['surface_n14'][0],data2['surface_n14'][fasta_stop]/data2['surface_n14'][0],\
#      data1['surf_avg_omega_div_omega_crit'][fasta_stop],data2['surf_avg_omega_div_omega_crit'][fasta_stop]))

###DATA AT END OF FAST CASE A MASS TRANSFER
if (fasta_stop != k_h and fasta_flag == 1):
	print("Fast Case A mass transfer stops")
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][fasta_stop],data1['center_h1'][fasta_stop],data2['center_h1'][fasta_stop],data1['center_he4'][fasta_stop],data2['center_he4'][fasta_stop],\
      data1['period_days'][fasta_stop],data1['binary_separation'][fasta_stop],data1['star_mass'][fasta_stop],data2['star_mass'][fasta_stop],\
      data1['log_L'][fasta_stop],data2['log_L'][fasta_stop],data1['log_Teff'][fasta_stop],data2['log_Teff'][fasta_stop],data1['log_g'][fasta_stop],data2['log_g'][fasta_stop],\
      10**(data1['log_R'][fasta_stop]),10**(data2['log_R'][fasta_stop]),data1['rl_relative_overflow_1'][fasta_stop],data2['rl_relative_overflow_2'][fasta_stop],\
      data1['surface_he4'][fasta_stop],data2['surface_he4'][fasta_stop],data1['surface_n14'][fasta_stop]/data1['surface_n14'][0],data2['surface_n14'][fasta_stop]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][fasta_stop],data2['surf_avg_omega_div_omega_crit'][fasta_stop]))

###FIND IF THERE IS SLOW CASE A MASS TRANSFER
slowa_start = 0
slowa_flag = 0
for k in range(fasta_stop,k_h-1):
	if data1['lg_mtransfer_rate'][k] > -10 and data1['lg_mtransfer_rate'][k+1] > data1['lg_mtransfer_rate'][k]:
		slowa_start = k
		slowa_flag = 1
		print("Slow Case A mass transfer starts")
		break

###DATA AT ONSET OF SLOW CASE A MASS TRANSFER
if slowa_start > 0:
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][slowa_start],data1['center_h1'][slowa_start],data2['center_h1'][slowa_start],data1['center_he4'][slowa_start],data2['center_he4'][slowa_start],\
      data1['period_days'][slowa_start],data1['binary_separation'][slowa_start],data1['star_mass'][slowa_start],data2['star_mass'][slowa_start],\
      data1['log_L'][slowa_start],data2['log_L'][slowa_start],data1['log_Teff'][slowa_start],data2['log_Teff'][slowa_start],data1['log_g'][slowa_start],data2['log_g'][slowa_start],\
      10**(data1['log_R'][slowa_start]),10**(data2['log_R'][slowa_start]),data1['rl_relative_overflow_1'][slowa_start],data2['rl_relative_overflow_2'][slowa_start],\
      data1['surface_he4'][slowa_start],data2['surface_he4'][slowa_start],data1['surface_n14'][slowa_start]/data1['surface_n14'][0],data2['surface_n14'][slowa_start]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][slowa_start],data2['surf_avg_omega_div_omega_crit'][slowa_start]))

###FIND END OF SLOW CASE A MASS TRANSFER
slowa_stop = 0
for k in range(slowa_start,k_h):
	if data1['lg_mtransfer_rate'][k] < -10 and data1['center_h1'][k] < 0.04:  
		slowa_stop = k
		#print("Slow Case A mass transfer stops")
		break

###CHECK IF BINARY MERGES BEFORE SLOW CASE A MASS TRANSFER STOPS
if (slowa_stop == 0):
	slowa_stop = k_h

###CHECK IF CONTACT OCCURS DURING SLOW CASE A
contact = 0
for k in range(slowa_start,slowa_stop):
	if data1['rl_relative_overflow_1'][k] > -0.01 and data2['rl_relative_overflow_2'][k] > -0.01:
		contact = k
		print("Contact during slow Case A occurs")
		print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
	  %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
		      (data1['star_age'][contact],data1['center_h1'][contact],data2['center_h1'][contact],data1['center_he4'][contact],data2['center_he4'][contact],\
		      data1['period_days'][contact],data1['binary_separation'][contact],data1['star_mass'][contact],data2['star_mass'][contact],\
		      data1['log_L'][contact],data2['log_L'][contact],data1['log_Teff'][contact],data2['log_Teff'][contact],data1['log_g'][contact],data2['log_g'][contact],\
		      10**(data1['log_R'][contact]),10**(data2['log_R'][contact]),data1['rl_relative_overflow_1'][contact],data2['rl_relative_overflow_2'][contact],\
		      data1['surface_he4'][contact],data2['surface_he4'][contact],data1['surface_n14'][contact]/data1['surface_n14'][0],data2['surface_n14'][contact]/data2['surface_n14'][0],\
		      data1['surf_avg_omega_div_omega_crit'][contact],data2['surf_avg_omega_div_omega_crit'][contact]))
		break

###FIND IF CONTACT SEVERES DURING SLOW CASE A
nocnct = 0
for k in range(contact,slowa_stop):
	if contact > 0 and data2['rl_relative_overflow_2'][k] < -0.01:
		nocnct = k
		print("Contact during slow Case A stops")
		print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
	  %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
		      (data1['star_age'][nocnct],data1['center_h1'][nocnct],data2['center_h1'][nocnct],data1['center_he4'][nocnct],data2['center_he4'][nocnct],\
		      data1['period_days'][nocnct],data1['binary_separation'][nocnct],data1['star_mass'][nocnct],data2['star_mass'][nocnct],\
		      data1['log_L'][nocnct],data2['log_L'][nocnct],data1['log_Teff'][nocnct],data2['log_Teff'][nocnct],data1['log_g'][nocnct],data2['log_g'][nocnct],\
		      10**(data1['log_R'][nocnct]),10**(data2['log_R'][nocnct]),data1['rl_relative_overflow_1'][nocnct],data2['rl_relative_overflow_2'][nocnct],\
		      data1['surface_he4'][nocnct],data2['surface_he4'][nocnct],data1['surface_n14'][nocnct]/data1['surface_n14'][0],data2['surface_n14'][nocnct]/data2['surface_n14'][0],\
		      data1['surf_avg_omega_div_omega_crit'][nocnct],data2['surf_avg_omega_div_omega_crit'][nocnct]))
		break

###DATA AT END OF SLOW CASE A MASS TRANSFER
if (slowa_stop != k_h and slowa_flag == 1):
	print("Slow Case A mass transfer stops")
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][slowa_stop],data1['center_h1'][slowa_stop],data2['center_h1'][slowa_stop],data1['center_he4'][slowa_stop],data2['center_he4'][slowa_stop],\
      data1['period_days'][slowa_stop],data1['binary_separation'][slowa_stop],data1['star_mass'][slowa_stop],data2['star_mass'][slowa_stop],\
      data1['log_L'][slowa_stop],data2['log_L'][slowa_stop],data1['log_Teff'][slowa_stop],data2['log_Teff'][slowa_stop],data1['log_g'][slowa_stop],data2['log_g'][slowa_stop],\
      10**(data1['log_R'][slowa_stop]),10**(data2['log_R'][slowa_stop]),data1['rl_relative_overflow_1'][slowa_stop],data2['rl_relative_overflow_2'][slowa_stop],\
      data1['surface_he4'][slowa_stop],data2['surface_he4'][slowa_stop],data1['surface_n14'][slowa_stop]/data1['surface_n14'][0],data2['surface_n14'][slowa_stop]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][slowa_stop],data2['surf_avg_omega_div_omega_crit'][slowa_stop]))

###DATA AT TAMS OR JUST BEFORE MS MERGER
if(merge_ms == 0):
	print("Binary and stellar parameters at TAMS of primary")
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][k_h],data1['center_h1'][k_h],data2['center_h1'][k_h],data1['center_he4'][k_h],data2['center_he4'][k_h],\
      data1['period_days'][k_h],data1['binary_separation'][k_h],data1['star_mass'][k_h],data2['star_mass'][k_h],\
      data1['log_L'][k_h],data2['log_L'][k_h],data1['log_Teff'][k_h],data2['log_Teff'][k_h],data1['log_g'][k_h],data2['log_g'][k_h],\
      10**(data1['log_R'][k_h]),10**(data2['log_R'][k_h]),data1['rl_relative_overflow_1'][k_h],data2['rl_relative_overflow_2'][k_h],\
      data1['surface_he4'][k_h],data2['surface_he4'][k_h],data1['surface_n14'][k_h]/data1['surface_n14'][0],data2['surface_n14'][k_h]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][k_h],data2['surf_avg_omega_div_omega_crit'][k_h]))

if(merge_ms == 1):
	print("Binary merges before end of core hydrogen burning")
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][k_h],data1['center_h1'][k_h],data2['center_h1'][k_h],data1['center_he4'][k_h],data2['center_he4'][k_h],\
      data1['period_days'][k_h],data1['binary_separation'][k_h],data1['star_mass'][k_h],data2['star_mass'][k_h],\
      data1['log_L'][k_h],data2['log_L'][k_h],data1['log_Teff'][k_h],data2['log_Teff'][k_h],data1['log_g'][k_h],data2['log_g'][k_h],\
      10**(data1['log_R'][k_h]),10**(data2['log_R'][k_h]),data1['rl_relative_overflow_1'][k_h],data2['rl_relative_overflow_2'][k_h],\
      data1['surface_he4'][k_h],data2['surface_he4'][k_h],data1['surface_n14'][k_h]/data1['surface_n14'][0],data2['surface_n14'][k_h]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][k_h],data2['surf_avg_omega_div_omega_crit'][k_h]))

###FIND START OF CORE HELIUM BURNING
i_he = 0
end = len(data1['star_age'])
for k in range(k_h,end-1):
	if data1['center_h1'][k] < 0.0001 and data1['center_he4'][k] < 0.98:
		i_he = k
		break

###FIND END OF CORE HELIUM BURNING
k_he = 0
end = len(data1['star_age'])
for k in range(k_h,end-1):
	if data1['center_h1'][k] < 0.0001 and data1['center_he4'][k] < 0.01:
		k_he = k
		break

###CHECK IF BINARY SURVIVES TILL CORE HELIUM INGITION
merge_ihe = 0
if (i_he == 0):
	i_he = len(data1['star_age'])-1
	merge_ihe = 1
        #print(merge_ihe)

###CHECK IF BINARY SURVIVES TILL END OF CORE HELIUM BURNING OF PRIMARY
merge_pms = 0
if (k_he == 0):
	k_he = len(data1['star_age'])-1
	merge_pms = 1

###FIND IF THERE IS CASE B MASS TRANSFER PHASE BETWEEN CORE HYDROGEN EXHAUSION AND CORE HELIUM IGNITION
caseb_start = 0
caseb_flag = 0
for k in range(k_h,i_he):
	if data1['lg_mtransfer_rate'][k] > -10:
		caseb_start = k
		caseb_flag = 1
		if fasta_flag == 1:
			print("Case AB mass transfer starts before He ignition")
		if fasta_flag == 0:
			print("Case B mass transfer starts before He ignition")
		break

###DATA AT ONSET OF CASE B MASS TRANSFER
if (caseb_start > 0 and merge_ms == 0):
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][caseb_start],data1['center_h1'][caseb_start],data2['center_h1'][caseb_start],data1['center_he4'][caseb_start],data2['center_he4'][caseb_start],\
      data1['period_days'][caseb_start],data1['binary_separation'][caseb_start],data1['star_mass'][caseb_start],data2['star_mass'][caseb_start],\
      data1['log_L'][caseb_start],data2['log_L'][caseb_start],data1['log_Teff'][caseb_start],data2['log_Teff'][caseb_start],data1['log_g'][caseb_start],data2['log_g'][caseb_start],\
      10**(data1['log_R'][caseb_start]),10**(data2['log_R'][caseb_start]),data1['rl_relative_overflow_1'][caseb_start],data2['rl_relative_overflow_2'][caseb_start],\
      data1['surface_he4'][caseb_start],data2['surface_he4'][caseb_start],data1['surface_n14'][caseb_start]/data1['surface_n14'][0],data2['surface_n14'][caseb_start]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][caseb_start],data2['surf_avg_omega_div_omega_crit'][caseb_start]))


###FIND END OF CASE B MASS TRANSFER
caseb_stop = 0
for k in range(caseb_start,i_he):
	if data1['lg_mtransfer_rate'][k] < -10:
		caseb_stop = k
                #print(caseb_stop)
		break

###CHECK IF BINARY MERGES BEFORE CASE B MASS TRANSFER STOPS
if (caseb_stop == 0):
	caseb_stop = i_he

###DATA AT END OF CASE B MASS TRANSFER
if (caseb_stop != i_he and merge_ms == 0 and caseb_flag == 1):
	if fasta_flag == 1:
		print("Case AB mass transfer stops before He ignition")
	if fasta_flag == 0:
		print("Case B mass transfer stops before He ignition")
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][caseb_stop],data1['center_h1'][caseb_stop],data2['center_h1'][caseb_stop],data1['center_he4'][caseb_stop],data2['center_he4'][caseb_stop],\
      data1['period_days'][caseb_stop],data1['binary_separation'][caseb_stop],data1['star_mass'][caseb_stop],data2['star_mass'][caseb_stop],\
      data1['log_L'][caseb_stop],data2['log_L'][caseb_stop],data1['log_Teff'][caseb_stop],data2['log_Teff'][caseb_stop],data1['log_g'][caseb_stop],data2['log_g'][caseb_stop],\
      10**(data1['log_R'][caseb_stop]),10**(data2['log_R'][caseb_stop]),data1['rl_relative_overflow_1'][caseb_stop],data2['rl_relative_overflow_2'][caseb_stop],\
      data1['surface_he4'][caseb_stop],data2['surface_he4'][caseb_stop],data1['surface_n14'][caseb_stop]/data1['surface_n14'][0],data2['surface_n14'][caseb_stop]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][caseb_stop],data2['surf_avg_omega_div_omega_crit'][caseb_stop]))

###DATA IF BINARY MERGES BEFORE END OF CASE B MASS TRANSFER
if (caseb_stop == i_he and merge_ms == 0 and caseb_flag == 1 and fasta_flag == 0):
	print("Binary merges during Case B mass transfer")
        #print(data2['center_h1'][caseb_stop])
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][caseb_stop],data1['center_h1'][caseb_stop],data2['center_h1'][caseb_stop],data1['center_he4'][caseb_stop],data2['center_he4'][caseb_stop],\
      data1['period_days'][caseb_stop],data1['binary_separation'][caseb_stop],data1['star_mass'][caseb_stop],data2['star_mass'][caseb_stop],\
      data1['log_L'][caseb_stop],data2['log_L'][caseb_stop],data1['log_Teff'][caseb_stop],data2['log_Teff'][caseb_stop],data1['log_g'][caseb_stop],data2['log_g'][caseb_stop],\
      10**(data1['log_R'][caseb_stop]),10**(data2['log_R'][caseb_stop]),data1['rl_relative_overflow_1'][caseb_stop],data2['rl_relative_overflow_2'][caseb_stop],\
      data1['surface_he4'][caseb_stop],data2['surface_he4'][caseb_stop],data1['surface_n14'][caseb_stop]/data1['surface_n14'][0],data2['surface_n14'][caseb_stop]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][caseb_stop],data2['surf_avg_omega_div_omega_crit'][caseb_stop]))

###DATA AT CORE HELIUM IGNITION
if(merge_ihe == 0 and merge_ms == 0):
	print("Core Helium burning starts in primary")
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][i_he],data1['center_h1'][i_he],data2['center_h1'][i_he],data1['center_he4'][i_he],data2['center_he4'][i_he],\
      data1['period_days'][i_he],data1['binary_separation'][i_he],data1['star_mass'][i_he],data2['star_mass'][i_he],\
      data1['log_L'][i_he],data2['log_L'][i_he],data1['log_Teff'][i_he],data2['log_Teff'][i_he],data1['log_g'][i_he],data2['log_g'][i_he],\
      10**(data1['log_R'][i_he]),10**(data2['log_R'][i_he]),data1['rl_relative_overflow_1'][i_he],data2['rl_relative_overflow_2'][i_he],\
      data1['surface_he4'][i_he],data2['surface_he4'][i_he],data1['surface_n14'][i_he]/data1['surface_n14'][0],data2['surface_n14'][i_he]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][i_he],data2['surf_avg_omega_div_omega_crit'][i_he]))

###FIND IF THERE IS CASE B MASS TRANSFER PHASE BETWEEN CORE HELIUM IGNITION AND CORE HELIUM EXHAUSION
caseb_start = 0
caseb_flag = 0
for k in range(i_he,k_he):
	if data1['lg_mtransfer_rate'][k] > -10:
		caseb_start = k
		caseb_flag = 1
		print("Case B mass transfer starts after He ignition")
		break

###DATA AT ONSET OF CASE B MASS TRANSFER
if (caseb_start > 0 and merge_ms == 0):
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][caseb_start],data1['center_h1'][caseb_start],data2['center_h1'][caseb_start],data1['center_he4'][caseb_start],data2['center_he4'][caseb_start],\
      data1['period_days'][caseb_start],data1['binary_separation'][caseb_start],data1['star_mass'][caseb_start],data2['star_mass'][caseb_start],\
      data1['log_L'][caseb_start],data2['log_L'][caseb_start],data1['log_Teff'][caseb_start],data2['log_Teff'][caseb_start],data1['log_g'][caseb_start],data2['log_g'][caseb_start],\
      10**(data1['log_R'][caseb_start]),10**(data2['log_R'][caseb_start]),data1['rl_relative_overflow_1'][caseb_start],data2['rl_relative_overflow_2'][caseb_start],\
      data1['surface_he4'][caseb_start],data2['surface_he4'][caseb_start],data1['surface_n14'][caseb_start]/data1['surface_n14'][0],data2['surface_n14'][caseb_start]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][caseb_start],data2['surf_avg_omega_div_omega_crit'][caseb_start]))


###FIND END OF CASE B MASS TRANSFER
caseb_stop = 0
for k in range(caseb_start,k_he):
	if data1['lg_mtransfer_rate'][k] < -10:
		caseb_stop = k
		break

###CHECK IF BINARY MERGES BEFORE CASE B MASS TRANSFER STOPS
if (caseb_stop == 0):
	caseb_stop = k_he

###DATA AT END OF CASE B MASS TRANSFER
if (caseb_stop != k_he and merge_ms == 0 and caseb_flag == 1):
	print("Case B mass transfer stops")
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][caseb_stop],data1['center_h1'][caseb_stop],data2['center_h1'][caseb_stop],data1['center_he4'][caseb_stop],data2['center_he4'][caseb_stop],\
      data1['period_days'][caseb_stop],data1['binary_separation'][caseb_stop],data1['star_mass'][caseb_stop],data2['star_mass'][caseb_stop],\
      data1['log_L'][caseb_stop],data2['log_L'][caseb_stop],data1['log_Teff'][caseb_stop],data2['log_Teff'][caseb_stop],data1['log_g'][caseb_stop],data2['log_g'][caseb_stop],\
      10**(data1['log_R'][caseb_stop]),10**(data2['log_R'][caseb_stop]),data1['rl_relative_overflow_1'][caseb_stop],data2['rl_relative_overflow_2'][caseb_stop],\
      data1['surface_he4'][caseb_stop],data2['surface_he4'][caseb_stop],data1['surface_n14'][caseb_stop]/data1['surface_n14'][0],data2['surface_n14'][caseb_stop]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][caseb_stop],data2['surf_avg_omega_div_omega_crit'][caseb_stop]))

###DATA AT END OF CASE B MASS TRANSFER
if (caseb_stop == k_he and merge_ms == 0 and caseb_flag == 1 and fasta_flag == 0):
	print("Binary merges during Case B mass transfer")
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][caseb_stop],data1['center_h1'][caseb_stop],data2['center_h1'][caseb_stop],data1['center_he4'][caseb_stop],data2['center_he4'][caseb_stop],\
      data1['period_days'][caseb_stop],data1['binary_separation'][caseb_stop],data1['star_mass'][caseb_stop],data2['star_mass'][caseb_stop],\
      data1['log_L'][caseb_stop],data2['log_L'][caseb_stop],data1['log_Teff'][caseb_stop],data2['log_Teff'][caseb_stop],data1['log_g'][caseb_stop],data2['log_g'][caseb_stop],\
      10**(data1['log_R'][caseb_stop]),10**(data2['log_R'][caseb_stop]),data1['rl_relative_overflow_1'][caseb_stop],data2['rl_relative_overflow_2'][caseb_stop],\
      data1['surface_he4'][caseb_stop],data2['surface_he4'][caseb_stop],data1['surface_n14'][caseb_stop]/data1['surface_n14'][0],data2['surface_n14'][caseb_stop]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][caseb_stop],data2['surf_avg_omega_div_omega_crit'][caseb_stop]))

###CHECK FOR INVERSE MASS TRANSFER POST HYDROGEN DEPLETION
k_inv = 0
for k in range(k_h,k_he):
        if data1['center_h1'][k] < 0.0001 and data1['rl_relative_overflow_2'][k] > -0.0001:
                k_inv = k
                print("Inverse mass transfer post core hydrogen burning of primary, binary assumed to merge")
                print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f \
       %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
                      (data1['star_age'][k_inv],data1['center_h1'][k_inv],data2['center_h1'][k_inv],data1['center_he4'][k_inv],data2['center_he4'][k_inv],\
                      data1['period_days'][k_inv],data1['binary_separation'][k_inv],data1['star_mass'][k_inv],data2['star_mass'][k_inv],\
                      data1['log_L'][k_inv],data2['log_L'][k_inv],data1['log_Teff'][k_inv],data2['log_Teff'][k_inv],data1['log_g'][k_inv],data2['log_g'][k_inv],\
                      10**(data1['log_R'][k_inv]),10**(data2['log_R'][k_inv]),data1['rl_relative_overflow_1'][k_inv],data2['rl_relative_overflow_2'][k_inv],\
                      data1['surface_he4'][k_inv],data2['surface_he4'][k_inv],data1['surface_n14'][k_inv]/data1['surface_n14'][0],data2['surface_n14'][k_inv]/data2['surface_n14'][0],\
                      data1['surf_avg_omega_div_omega_crit'][k_inv],data2['surf_avg_omega_div_omega_crit'][k_inv]))
                break


###DATA AT CORE HELIUM EXHAUSION
if(merge_ihe == 0 and merge_pms == 0 and merge_ms == 0):
	print("End of core Helium burning in primary")
	print("%6.2f  %5.2f  %5.2f  %5.2f  %5.2f %8.2f  %7.2f %6.2f %6.2f %6.2f %6.2f%6.2f%6.2f  %6.2f  %6.2f %6.1f %6.1f  \
      %+5.2f        %+5.2f %5.2f %5.2f   %6.2f   %6.2f        %5.2f        %5.2f"%\
      (data1['star_age'][k_he],data1['center_h1'][k_he],data2['center_h1'][k_he],data1['center_he4'][k_he],data2['center_he4'][k_he],\
      data1['period_days'][k_he],data1['binary_separation'][k_he],data1['star_mass'][k_he],data2['star_mass'][k_he],\
      data1['log_L'][k_he],data2['log_L'][k_he],data1['log_Teff'][k_he],data2['log_Teff'][k_he],data1['log_g'][k_he],data2['log_g'][k_he],\
      10**(data1['log_R'][k_he]),10**(data2['log_R'][k_he]),data1['rl_relative_overflow_1'][k_he],data2['rl_relative_overflow_2'][k_he],\
      data1['surface_he4'][k_he],data2['surface_he4'][k_he],data1['surface_n14'][k_he]/data1['surface_n14'][0],data2['surface_n14'][k_he]/data2['surface_n14'][0],\
      data1['surf_avg_omega_div_omega_crit'][k_he],data2['surf_avg_omega_div_omega_crit'][k_he]))






