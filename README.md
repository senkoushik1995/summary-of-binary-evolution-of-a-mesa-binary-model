Hello, welcome tp the README file! The following paragraphs contain instructions on how to use the binary.py script to get a brief overview of the evolution of a MESA binary model.

Requirements:

1. The LOGS1 and LOGS2 folders must contain the history.data files.

2. The LOGS1/history.data file must contain the following columns of data: 'star_age','star_mass','log_Teff','log_L','log_R','log_g','center_h1','center_he4','center_c12','lg_mtransfer_rate',
                                                                           'surf_avg_omega_div_omega_crit','surface_h1','surface_he4','surface_c12','surface_n14','surf_avg_v_rot','v_orb_1',
                                                                           'rl_relative_overflow_1','rl_relative_overflow_2','he_core_mass','c_core_mass','period_days','binary_separation'

3. The LOGS2/history.data file must contain the following columns of data: 'star_age','star_mass','log_Teff','log_L','log_R','log_g','center_h1','center_he4','center_c12','lg_mtransfer_rate',
                                                                           'surf_avg_omega_div_omega_crit','surface_h1','surface_he4','surface_c12','surface_n14','surf_avg_v_rot','v_orb_2',
                                                                           'rl_relative_overflow_1','rl_relative_overflow_2','he_core_mass','c_core_mass','period_days','binary_separation'

Run:

To run the script, do: python binary.py /full_path_to_directory_upto_the_LOGS_files/

for example, if the LOGS1 and LOGS2 folders are in /Data/MESA/binary/test/ then use the command "python binary.py /Data/MESA/binary/test/"
