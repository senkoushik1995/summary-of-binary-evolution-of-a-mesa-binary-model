import sys
import numpy as np
import pandas as pd 




dir_name = sys.argv[1]

file1 = dir_name+'LOGS1/history.data'
file2 = dir_name+'LOGS2/history.data'


print ("Primary history file: %s"%(file1))
print ("Secondary history file: %s"%(file2))

###LOAD DATA FILES
data1 = pd.read_csv(file1,skiprows=5,\
                    delim_whitespace=True,\
                    usecols=['star_age','star_mass','log_Teff','log_L','log_R','log_g','center_h1','center_he4','center_c12','lg_mtransfer_rate',\
                             'surf_avg_omega_div_omega_crit','surface_h1','surface_he4','surface_c12','surface_n14','surf_avg_v_rot','v_orb_1',\
                             'rl_relative_overflow_1','he_core_mass','c_core_mass','period_days','binary_separation'])        
data1['star_age'] = data1['star_age']/1000000
file2=dir_name+'/LOGS2/history.data'
data2 = pd.read_csv(file2,skiprows=5,\
                    delim_whitespace=True,\
                    usecols=['star_age','star_mass','log_Teff','log_L','log_R','log_g','center_h1','center_he4','center_c12','lg_mtransfer_rate',\
                             'surf_avg_omega_div_omega_crit','surface_h1','surface_he4','surface_c12','surface_n14','surf_avg_v_rot','v_orb_2',\
                             'rl_relative_overflow_2','he_core_mass','c_core_mass','period_days','binary_separation'])
data2['star_age'] = data2['star_age']/1000000

###OUTPUT DATA FOR PRIMARY
print("***********************Primary*****************************")
print("Age[Myrs]    Xc    Yc   orbP[days]   a[Rs]   Mass[Ms]   logL[Ls]   logTeff[K]   logg[cgs]   R[Rs]   (R-RL)/RL    Ys   Ns/N0   v/v_crit")


###DATA AT ZAMS
#print("Stellar and binary parameters at ZAMS")
print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][0],\
      data1['center_h1'][0],data1['center_he4'][0],data1['period_days'][0],data1['binary_separation'][0],data1['star_mass'][0],data1['log_L'][0],data1['log_Teff'][0],\
      data1['log_g'][0],10**(data1['log_R'][0]),data1['rl_relative_overflow_1'][0],data1['surface_he4'][0],data1['surface_n14'][0]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][0]))

###FIND END OF CORE HYDROGEN BURNING OF PRIMARY
k_h = 0
end = len(data1['star_age'])
for k in range(0,end-1):
	if data1['center_h1'][k] < 0.01:
		k_h = k
		break

###CHECK IF BINARY SURVIVES TILL END OF CORE HYDROGEN BURNING OF PRIMARY
merge_ms = 0
if (k_h == 0):
	k_h = len(data1['star_age'])-1
	merge_ms = 1

###FIND IF THERE IS FAST CASE A MASS TRANSFER
fasta_start = 0
for k in range(0,k_h):
	if data1['lg_mtransfer_rate'][k] > -5:
		fasta_start = k
		print("Fast Case A mass transfer starts")
		break

###DATA AT ONSET OF FAST CASE A MASS TRANSFER
print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][fasta_start],\
      data1['center_h1'][fasta_start],data1['center_he4'][fasta_start],data1['period_days'][fasta_start],data1['binary_separation'][fasta_start],\
      data1['star_mass'][fasta_start],data1['log_L'][fasta_start],data1['log_Teff'][fasta_start],data1['log_g'][fasta_start],10**(data1['log_R'][fasta_start]),\
      data1['rl_relative_overflow_1'][fasta_start],data1['surface_he4'][fasta_start],data1['surface_n14'][fasta_start]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][fasta_start]))

###FIND END OF FAST CASE A MASS TRANSFER
fasta_stop = 0
for k in range(fasta_start,k_h):
	if data1['lg_mtransfer_rate'][k] < -5:
		fasta_stop = k
		#print("Fast Case A mass transfer stops")
		break

###CHECK IF CONTACT OCCURS DURING FAST CASE A
contact = 0
for k in range(fasta_start,fasta_stop):
	if data1['rl_relative_overflow_1'][k] > -0.02 and data2['rl_relative_overflow_2'][k] > -0.02:
		contact = k
		print("Contact during fast Case A occurs")
		print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][contact],\
                      data1['center_h1'][contact],data1['center_he4'][contact],data1['period_days'][contact],data1['binary_separation'][contact],\
                      data1['star_mass'][contact],data1['log_L'][contact],data1['log_Teff'][contact],data1['log_g'][contact],10**(data1['log_R'][contact]),\
                      data1['rl_relative_overflow_1'][contact],data1['surface_he4'][contact],data1['surface_n14'][contact]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][contact]))
		break

###FIND IF CONTACT SEVERES DURING FAST CASE A
nocnct = 0
for k in range(contact,fasta_stop):
	if data2['rl_relative_overflow_2'][k] < -0.02:
		nocnct = k
		print("Contact during fast Case A stops")
		print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][nocnct],\
                      data1['center_h1'][nocnct],data1['center_he4'][nocnct],data1['period_days'][nocnct],data1['binary_separation'][nocnct],\
                      data1['star_mass'][nocnct],data1['log_L'][nocnct],data1['log_Teff'][nocnct],data1['log_g'][nocnct],10**(data1['log_R'][nocnct]),\
                      data1['rl_relative_overflow_1'][nocnct],data1['surface_he4'][nocnct],data1['surface_n14'][nocnct]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][nocnct]))
		break

###DATA AT END OF FAST CASE A MASS TRANSFER
print("Fast Case A mass transfer stops")
print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][fasta_stop],\
      data1['center_h1'][fasta_stop],data1['center_he4'][fasta_stop],data1['period_days'][fasta_stop],data1['binary_separation'][fasta_stop],\
      data1['star_mass'][fasta_stop],data1['log_L'][fasta_stop],data1['log_Teff'][fasta_stop],data1['log_g'][fasta_stop],10**(data1['log_R'][fasta_stop]),\
      data1['rl_relative_overflow_1'][fasta_stop],data1['surface_he4'][fasta_stop],data1['surface_n14'][fasta_stop]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][fasta_stop]))

###FIND IF THERE IS SLOW CASE A MASS TRANSFER
slowa_start = 0
for k in range(fasta_stop,k_h):
	if data1['lg_mtransfer_rate'][k] > -10:
		slowa_start = k
		print("Slow Case A mass transfer starts")
		break

###DATA AT ONSET OF SLOW CASE A MASS TRANSFER
print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][slowa_start],\
      data1['center_h1'][slowa_start],data1['center_he4'][slowa_start],data1['period_days'][slowa_start],data1['binary_separation'][slowa_start],\
      data1['star_mass'][slowa_start],data1['log_L'][slowa_start],data1['log_Teff'][slowa_start],data1['log_g'][slowa_start],10**(data1['log_R'][slowa_start]),\
      data1['rl_relative_overflow_1'][slowa_start],data1['surface_he4'][slowa_start],data1['surface_n14'][slowa_start]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][slowa_start]))

###FIND END OF SLOW CASE A MASS TRANSFER
slowa_stop = 0
for k in range(slowa_start,k_h-100):
	if data1['lg_mtransfer_rate'][k] < -10 and data1['lg_mtransfer_rate'][k+100] < -10: #+100 timesteps added to remove very short stoppages in slow Case A mass transfer 
		slowa_stop = k
		#print("Slow Case A mass transfer stops")
		break

###CHECK IF BINARY MERGES BEFORE SLOW CASE A MASS TRANSFER STOPS
if (slowa_stop == 0):
	slowa_stop = k_h

###CHECK IF CONTACT OCCURS DURING SLOW CASE A
contact = 0
for k in range(slowa_start,slowa_stop):
	if data1['rl_relative_overflow_1'][k] > -0.02 and data2['rl_relative_overflow_2'][k] > -0.02:
		contact = k
		print("Contact during slow Case A occurs")
		print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][contact],\
                      data1['center_h1'][contact],data1['center_he4'][contact],data1['period_days'][contact],data1['binary_separation'][contact],\
                      data1['star_mass'][contact],data1['log_L'][contact],data1['log_Teff'][contact],data1['log_g'][contact],10**(data1['log_R'][contact]),\
                      data1['rl_relative_overflow_1'][contact],data1['surface_he4'][contact],data1['surface_n14'][contact]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][contact]))
		break

###FIND IF CONTACT SEVERES DURING SLOW CASE A
nocnct = 0
for k in range(contact,slowa_stop):
	if contact > 0 and data2['rl_relative_overflow_2'][k] < -0.02:
		nocnct = k
		print("Contact during slow Case A stops")
		print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][nocnct],\
                      data1['center_h1'][nocnct],data1['center_he4'][nocnct],data1['period_days'][nocnct],data1['binary_separation'][nocnct],\
                      data1['star_mass'][nocnct],data1['log_L'][nocnct],data1['log_Teff'][nocnct],data1['log_g'][nocnct],10**(data1['log_R'][nocnct]),\
                      data1['rl_relative_overflow_1'][nocnct],data1['surface_he4'][nocnct],data1['surface_n14'][nocnct]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][nocnct]))
		break

###DATA AT END OF SLOW CASE A MASS TRANSFER
if (slowa_stop != k_h):
	print("Slow Case A mass transfer stops")
	print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][slowa_stop],\
	      data1['center_h1'][slowa_stop],data1['center_he4'][slowa_stop],data1['period_days'][slowa_stop],data1['binary_separation'][slowa_stop],\
	      data1['star_mass'][slowa_stop],data1['log_L'][slowa_stop],data1['log_Teff'][slowa_stop],data1['log_g'][slowa_stop],10**(data1['log_R'][slowa_stop]),\
	      data1['rl_relative_overflow_1'][slowa_stop],data1['surface_he4'][slowa_stop],data1['surface_n14'][slowa_stop]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][slowa_stop]))

###DATA AT TAMS OR JUST BEFORE MS MERGER
if(merge_ms == 0):
	print("Binary and stellar parameters at TAMS of primary")
	print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][k_h],\
	      data1['center_h1'][k_h],data1['center_he4'][k_h],data1['period_days'][k_h],data1['binary_separation'][k_h],data1['star_mass'][k_h],data1['log_L'][k_h],data1['log_Teff'][k_h],\
	      data1['log_g'][k_h],10**(data1['log_R'][k_h]),data1['rl_relative_overflow_1'][k_h],data1['surface_he4'][k_h],data1['surface_n14'][k_h]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][k_h]))

if(merge_ms == 1):
	print("Binary merges before end of core hydrogen burning")
	print("     %.2f  %.2f  %.2f         %.2f   %.2f      %.2f       %.2f         %.2f        %.2f    %.2f       %+.2f  %.2f    %.2f       %.2f"%(data1['star_age'][k_h],\
	      data1['center_h1'][k_h],data1['center_he4'][k_h],data1['period_days'][k_h],data1['binary_separation'][k_h],data1['star_mass'][k_h],data1['log_L'][k_h],data1['log_Teff'][k_h],\
	      data1['log_g'][k_h],10**(data1['log_R'][k_h]),data1['rl_relative_overflow_1'][k_h],data1['surface_he4'][k_h],data1['surface_n14'][k_h]/data1['surface_n14'][0],data1['surf_avg_omega_div_omega_crit'][k_h]))

###FIND START OF CORE HELIUM BURNING
